# sisop-praktikum-modul-4-2023-BJ-U07

## Modul 4 Sisop 2023 U07 Formal Report

Group Members:
1. Khairiya Maisa Putri (5025211192)
2. Fauzan Ahmad Faisal (5025211067)
3. Muhammad Fadhlan Ashila Harashta (5025211068)

# Question Package
Yakiniku Washabi

# No. 1
## Question

> Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.
>- Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.
**kaggle datasets download -d bryanb/fifa-player-stats-database**
Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 
>- Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama **FIFA23_official_data.csv** dan mencetak data pemain yang **berusia di bawah 25 tahun**, memiliki **potensi di atas 85**, dan bermain di **klub lain selain Manchester City**. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.
>- Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
>- Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
>- Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.
>> **Catatan:** 
>>- Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
>>- Perhatikan port  pada masing-masing instance.

## A
The first thing that I did was made a file called **storage.c** where in that file, we will ```download``` the kaggle database, and then ```unzip``` the database to get the files that we need.

## Explanation
```c
    // Execute the download command
   if (system("kaggle datasets download -d bryanb/fifa-player-stats-database") != 0) {
       printf("\e[31m\nFailed to download the dataset.\e[0m\n");
       return 1;
   }

   // Execute the extract command
   if (system("unzip fifa-player-stats-database.zip") != 0) {
       printf("\e[31m\nFailed to extract the dataset.\e[0m\n");
       return 1;
   }

   printf("\e[32m\nDataset downloaded and extracted successfully.\e[0m\n");
```
1. To download the database from Kaggle I used ```kaggle datasets download -d bryanb/fifa-player-stats-database``` command which is run by system.
2. If the kaggle database can't be downloaded, then a notification will be printed with a red color.
3. To unzip/extract the database that was downloaded, I used ```unzip fifa-player-stats-database.zip``` command which is run by system.
4. If the database can't be unzipped/extracted, the a notification will be printed with a red color.
5. And if the database is downloaded and extracted successfully, the code will print a notification with a green color.

## B
In the ```storage.c``` file make a program where we have to print out the data of players from the ```FIFA23_official_data.csv``` file where the **age** is **below 25**, the **potential** is **above 85**, and the **club** is **other than Manchester City**. And the data that we need to print as outputs are **Name, Age, Potential, Club, Nationality, and Photo**.

## Explanation
```c
typedef struct {
  char name[100];
  int age;
  int potential;
  char club[50];
  char nationality[100];
  char photo[100];
} Player;

```  
First thing I did was made a struct that was defined as ```Player``` data type, which represents information about a player on the .csv file. The struct has some fields which are ```Name, Age, Potential, Club, Nationality, and also Photo```.

```c
  FILE *file;
  char line[MAX_LINE_LENGTH];
  Player players[MAX_PLAYERS];
  int numPlayers = 0;

  file = fopen("FIFA23_official_data.csv", "r");
  if (file == NULL) {
     printf("\e[31m\nFailed to open the file.\e[0m\n");
     return 1;
  }

  fgets(line, sizeof(line), file); // Skip the first line (header)

  while (fgets(line, sizeof(line), file)) {
     char *token = strtok(line, ",");
     int fieldCount = 0;
     Player player;

```
- Declares a pointer to a `FILE` structure called `file`. This pointer will be used to represent a file.
- Declares a character array called `line` with a size of `MAX_LINE_LENGTH`. This array is used to store a line read from the file.
- Declares an array of `Player` structs called `players` with a size of `MAX_PLAYERS`. This array is used to store player information.
- Initializes an integer variable `numPlayers` to 0. This variable will keep track of the number of players processed.
- Uses the `fopen` function to open the file named "FIFA23_official_data.csv" in read mode ("r"). The `fopen` function returns a pointer to the opened file. If the file fails to open (returns `NULL`), it prints an error message with the red color and returns 1 to indicate failure.
- Uses the `fgets` function to read the first line (header) from the file and store it in the `line` array. This line is skipped and not processed further.
- Enters a `while` loop that continues until there are no more lines to read from the file. The `fgets` function is used to read each line from the file and store it in the `line` array.
- Declares a character pointer `token` and initializes it with the result of `strtok(line, ",")`. This function tokenizes the line using commas as delimiters and returns a pointer to the first token. It also modifies the `line` array, replacing the first comma encountered with a null character ('\0').
- Declares an integer variable `fieldCount` and initializes it to 0. This variable keeps track of the current field being processed.
- Declares a `Player` struct variable called `player`. This variable is used to store the information of an individual player.

```c
while (token != NULL && fieldCount <= 8) {
        switch (fieldCount) {
           case 1:
              strncpy(player.name, token, sizeof(player.name));
              break;
           case 2:
              player.age = atoi(token);
              break;
           case 7:
              player.potential = atoi(token);
              break;
           case 8:
              strncpy(player.club, token, sizeof(player.club));
              break;
           case 4:
              strncpy(player.nationality, token, sizeof(player.nationality));
              break;
           case 3:
              strncpy(player.photo, token, sizeof(player.photo));
              break;
        }
        token = strtok(NULL, ",");
        fieldCount++;
     }
```
- Enters an inner `while` loop that continues until the `token` is `NULL` or the `fieldCount` exceeds 8. In each iteration, it processes a single field of player information.
- Within the `switch` statement, based on the value of `fieldCount`, it assigns the tokenized value to the corresponding field of the `player` struct. The `strncpy` function is used to copy the token into the corresponding field, and `atoi` function is used to convert the token into an integer when needed.
- After processing a field, it updates the `token` pointer using `strtok(NULL, ",")` to get the next token. It also increments the `fieldCount` by 1.

```c
     if (fieldCount > 0 && player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
        if (numPlayers >= MAX_PLAYERS) {
           printf("\e[31m\nMaximum number of players reached.\e[0m\n");
           break;
        }
        players[numPlayers++] = player;
     }
  }
  fclose(file);

  for (int i = 0; i < numPlayers; i++) {
   printf("\e[36mName:\e[0m %s\n", players[i].name);
   printf("\e[36mAge:\e[0m %d\n", players[i].age);
   printf("\e[36mPotential:\e[0m %d\n", players[i].potential);
   printf("\e[36mClub:\e[0m %s\n", players[i].club);
   printf("\e[36mNationality:\e[0m %s\n", players[i].nationality);
   printf("\e[36mPhoto:\e[0m %s\n", players[i].photo);
   printf("\e[33m--------------------\e[33m\n");
  }
  return 0;
}
```
- If the field count is greater than 0 and the player meets certain criteria (age, potential, and club conditions), it adds the `player` struct to the `players` array. If the maximum number of players has been reached, it prints an **error** message with a red color and breaks out of the loop.
- Closes the file using the `fclose` function.
- The loop iterates over the `players` array and prints the information of each player with a cyan color.
- Returns 0 to indicate successful execution of the program.

**Full Source Code for storage.c File:**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

#define MAX_LINE_LENGTH 20000
#define MAX_PLAYERS 20000

typedef struct {
  char name[100];
  int age;
  int potential;
  char club[50];
  char nationality[100];
  char photo[100];
} Player;

int main() {

    // Execute the download command
   if (system("kaggle datasets download -d bryanb/fifa-player-stats-database") != 0) {
       printf("\e[31m\nFailed to download the dataset.\e[0m\n");
       return 1;
   }

   // Execute the extract command
   if (system("unzip fifa-player-stats-database.zip") != 0) {
       printf("\e[31m\nFailed to extract the dataset.\e[0m\n");
       return 1;
   }

   printf("\e[32m\nDataset downloaded and extracted successfully.\e[0m\n");

   FILE *file;
  char line[MAX_LINE_LENGTH];
  Player players[MAX_PLAYERS];
  int numPlayers = 0;

  file = fopen("FIFA23_official_data.csv", "r");
  if (file == NULL) {
     printf("\e[31m\nFailed to open the file.\e[0m\n");
     return 1;
  }

  fgets(line, sizeof(line), file); // Skip the first line (header)

  while (fgets(line, sizeof(line), file)) {
     char *token = strtok(line, ",");
     int fieldCount = 0;
     Player player;

     while (token != NULL && fieldCount <= 8) {
        switch (fieldCount) {
           case 1:
              strncpy(player.name, token, sizeof(player.name));
              break;
           case 2:
              player.age = atoi(token);
              break;
           case 7:
              player.potential = atoi(token);
              break;
           case 8:
              strncpy(player.club, token, sizeof(player.club));
              break;
           case 4:
              strncpy(player.nationality, token, sizeof(player.nationality));
              break;
           case 3:
              strncpy(player.photo, token, sizeof(player.photo));
              break;
        }
        token = strtok(NULL, ",");
        fieldCount++;
     }

     if (fieldCount > 0 && player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
        if (numPlayers >= MAX_PLAYERS) {
           printf("\e[31m\nMaximum number of players reached.\e[0m\n");
           break;
        }
        players[numPlayers++] = player;
     }
  }
  fclose(file);

  for (int i = 0; i < numPlayers; i++) {
   printf("\e[36mName:\e[0m %s\n", players[i].name);
   printf("\e[36mAge:\e[0m %d\n", players[i].age);
   printf("\e[36mPotential:\e[0m %d\n", players[i].potential);
   printf("\e[36mClub:\e[0m %s\n", players[i].club);
   printf("\e[36mNationality:\e[0m %s\n", players[i].nationality);
   printf("\e[36mPhoto:\e[0m %s\n", players[i].photo);
   printf("\e[33m--------------------\e[33m\n");
  }
  return 0;
}
```
## C
On this question we were asked to make a Dockerfile

```
# Use an appropriate base image with necessary dependencies
FROM ubuntu:latest
```
This line indicates that the **Docker image** will be created using the most **recent version of the Ubuntu distribution**, which is represented by the base image `"ubuntu:latest."`

```
# Install required packages
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    unzip \
    && pip3 install kaggle\
```
- `RUN`: This is a Dockerfile instruction that allows you to run commands inside the Docker image during the build process.
- `apt-get update`: This command updates the package lists on the system to ensure that the latest package information is available for installation.
- `apt-get install -y`: This command installs the specified packages. The -y flag is used to automatically answer "yes" to any prompts that may appear during the installation process, ensuring a non-interactive installation.
- `python3`: This is the package name for the Python 3 interpreter. It is being installed as a dependency for the application.
- `python3-pip`: This package provides the Pip package manager for Python 3. Pip is used to install Python packages and dependencies.
- `unzip`: This package provides the ability to extract and manipulate zip files. It may be needed for certain tasks or dependencies in the application.
- `pip3 install kaggle`: This command uses Pip to install the "kaggle" package. Kaggle is a platform that hosts machine learning competitions and provides datasets for data analysis and modeling. By installing the "kaggle" package, the image is being prepared to interact with the Kaggle API and access Kaggle resources.

```
# Copy the C program to the Docker image
COPY storage.c /app/storage.c

# Set the working directory
WORKDIR /app
```
These two lines of code copy the `storage.c` file from the local file system to the `/app` directory within the Docker image, and set the working directory to /app, which will be used as the base for subsequent instructions within the Dockerfile.
```
# Compile the C program
RUN gcc storage.c -o storage
```
This code block is compiling the `storage.c` C program using the GCC compiler inside the Docker image. The resulting executable binary file named `storage` will be generated and available for use within the Docker container.
```
# Set up Kaggle environment
RUN mkdir -p ~/.kaggle \
    && echo '{"username":"khairiyamaisaputri","key":"700549c5091c261a0bee7bd16bf67859"}' > ~/.kaggle/kaggle.json \
    && chmod 600 ~/.kaggle/kaggle.json
```
- `RUN`: This is a Dockerfile instruction that allows you to **run commands** inside the Docker image during the build process.
- `mkdir -p ~/.kaggle`: This command creates a directory named `.kaggle` in the user's home directory. The **-p** flag ensures that the parent directories are created if they don't exist.
- `echo '{"username":"khairiyamaisaputri","key":"700549c5091c261a0bee7bd16bf67859"}' > ~/.kaggle/kaggle.json`: This command writes the specified JSON content into a file named `kaggle.json` within the **~/.kaggle** directory. The JSON content includes the Kaggle username and API key required to authenticate and access Kaggle resources.
- `chmod 600 ~/.kaggle/kaggle.json`: This command changes the file permissions of **kaggle.json** to **600**, which means that only the file owner has read and write permissions, while other users have no permissions. This step is important for security purposes, as it restricts access to the Kaggle API credentials.
```
# Set the PATH environment variable
ENV PATH "$PATH:/root/.local/bin"
```
This code block sets the `PATH` environment variable in the Docker image. By appending **:/root/.local/bin** to the existing **PATH** value, it ensures that the directory **/root/.local/bin** is included in the search path for executable binaries. This can be useful if there are executables installed in that directory that need to be accessible from anywhere within the container.
```
# Run the C program when the container starts
CMD ["./storage"]
```
This code block sets the default command to run inside the container when it starts. It specifies that the `./storage` executable binary should be executed. This means that when the container is launched, it will automatically run the `./storage` program as its main process.

**The full source code for this question:**
```
# Use an appropriate base image with necessary dependencies
FROM ubuntu:latest

# Install required packages
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    unzip \
    && pip3 install kaggle

# Copy the C program to the Docker image
COPY storage.c /app/storage.c

# Set the working directory
WORKDIR /app

# Compile the C program
RUN gcc storage.c -o storage

# Set up Kaggle environment
RUN mkdir -p ~/.kaggle \
    && echo '{"username":"khairiyamaisaputri","key":"700549c5091c261a0bee7bd16bf67859"}' > ~/.kaggle/kaggle.json \
    && chmod 600 ~/.kaggle/kaggle.json

# Set the PATH environment variable
ENV PATH "$PATH:/root/.local/bin"

# Run the C program when the container starts
CMD ["./storage"]
```
## D
On this part of the question, we were asked to make a **Docker Hub** file in a form of .txt file.
```
https://hub.docker.com/r/khairiyamaisaputri/storage-app
```
## E
On this part of the question, we were asked to make a folder named Napoli and Barcelona. Subsequently, we must utilize Docker Compose to run the instance five times for each folder.

```yml
version: '3'
```
`version: '3'`: This line specifies the version of the Docker Compose file format being used. In this case, it's version 3.
```yml
services:
  Barcelona:
    build: ./Barcelona
    image: khairiyamaisaputri/storage-app:storage_img
    deploy:
      replicas: 5
```
- `services:`: This section defines the services or containers to be created within the Docker Compose environment.
- `Barcelona:`: This is the name of the first service or container defined. It represents the container for the Barcelona directory.
- `build: ./Barcelona`: This line specifies the path (**./Barcelona**) to the build context for the Barcelona container. The build context typically contains the necessary files and configurations for building the image for this container.
- `image: khairiyamaisaputri/storage-app:storage_img`: This line specifies the name and version tag of the Docker image to be used for the Barcelona container. In this case, it uses the image named **khairiyamaisaputri/storage-app** with the version tag **storage_img**.
- `deploy:`: This section provides deployment-related configuration options for the container.
- `replicas: 5`: This line specifies the number of replicas or instances to create for the Barcelona service. In this case, it deploys 5 instances of the container.
```yml
  Napoli:
    build: ./Napoli
    image: khairiyamaisaputri/storage-app:storage_img
    deploy:
      replicas: 5

```
- `Napoli:`: This is the name of the second service or container defined. It represents the container for the Napoli directory.
- `build: ./Napoli`: Similar to the Barcelona container, this line specifies the path to the build context for the Napoli container.
- `image: khairiyamaisaputri/storage-app:storage_img`: This line specifies the image to be used for the Napoli container, following the same format as for the Barcelona container.
- `deploy:`: This section provides deployment-related configuration options for the Napoli container.
- `replicas: 5`: This line specifies that 5 replicas or instances of the Napoli container should be deployed.

**Full source code for this question:**
```yml
version: '3'
services:
  Barcelona:
    build: ./Barcelona
    image: khairiyamaisaputri/storage-app:storage_img
    deploy:
      replicas: 5

  Napoli:
    build: ./Napoli
    image: khairiyamaisaputri/storage-app:storage_img
    deploy:
      replicas: 5
```
## Difficulty
- My ubuntu crash so many times because of the Dockerfile, and it often won't finish building


# No 1
## Question
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:
Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut. 
[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
Dengan keterangan sebagai berikut.
STATUS: SUCCESS atau FAILED.
dd: 2 digit tanggal.
MM: 2 digit bulan.
yyyy: 4 digit tahun.
HH: 24-hour clock hour, with a leading 0 (e.g. 22).
mm: 2 digit menit.
ss: 2 digit detik.
CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
DESC: keterangan action, yaitu:
[User]-Create directory x.
[User]-Rename from x to y.
[User]-Remove directory x.
[User]-Remove file x.

Contoh:
SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y

## Source code
```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

void makeLog(const char *res, const char *operation, const char *desc, const char *add);
const char *src = "/home/fadhlan/target/nanaxgerma/nanaxgerma/src_data";

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);
    res = lstat(newpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);

    printf("readdir %s\n", newpath);
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(newpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    printf("read %s\n", newpath);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(newpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static bool restricted(char *path) {
    bool restricted = false;
    char *copy = strdup(path);
    char *token = strtok(copy, "/");

    while (token != NULL) {
        if (strstr(token, "restricted") != NULL) {
            restricted = true;
        }
        if(strstr(token, "bypass") != NULL){
            restricted = false;
        }
        token = strtok(NULL, "/");
    }

    return restricted;
}


static int xmp_mkdir(const char *path, mode_t mode)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    // Check if the path or any parent directories are restricted
    if (restricted(newpath))
    {
        makeLog("FAILED", "MKDIR", newpath,"");
        return -EACCES; 
    }
    
    int res = mkdir(newpath, mode);

    if (res == -1)
    {
        //means failed
        makeLog("FAILED", "MKDIR", newpath,"");
        return -errno;
    }
    
    //success
    makeLog("SUCCESS", "MKDIR", newpath,"");
    return 0;
}

static int xmp_rmdir(const char *path)
{
    char newpath[1024];
    int res;
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -EACCES;
    }

    res = rmdir(newpath);
    if (res == -1){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -errno;
    }
    //make log success
    makeLog("SUCCESS", "RMDIR", newpath,"");
    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;
    char before[1024];
    strcpy(before, src);
    strcat(before, from);

    char new[1024];
    strcpy(new, src);
    strcat(new, to);

    if(restricted(before)){
        if(restricted(new)){
        makeLog("FAILED", "RENAME", before,new);
        return -EACCES;
        }
    }

    res = rename(before, new);

    if (res == -1){
        makeLog("FAILED", "RENAME", before,new);
        return -errno;
    }

    makeLog("SUCCESS", "RENAME", before,new);
    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -EACCES;
    }

    res = unlink(newpath);

    if (res == -1){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -errno;
    }
    makeLog("SUCCESS", "RMFILE", newpath,"");

    return 0;
}


static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if (restricted(newpath)) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -EACCES;
    }

    int fd = creat(newpath, mode);
    if (fd == -1) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -errno;
    }

    fi->fh = fd;

    makeLog("SUCCESS", "TOUCH", newpath, "");

    return 0;
}

void makeLog(const char *res, const char *operation, const char *desc, const char *add)
{
    time_t now;
    time(&now);
    struct tm *timestamp = localtime(&now);

    FILE *logging = fopen("/home/fadhlan/target/nanaxgerma/SinSeiFS.log", "a");

    char clock[20];
    strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

    if (logging != NULL)
    {
        fprintf(logging, "%s::%s::%s::%s-", res, clock, operation, desc);

        if (strcmp(operation, "MKDIR") == 0)
        {
            fprintf(logging, "Create directory %s\n", desc);
        }
        else if (strcmp(operation, "RENAME") == 0)
        {
            fprintf(logging, "Rename from %s to %s\n", desc, add);
        }
        else if (strcmp(operation, "RMDIR") == 0)
        {
            fprintf(logging, "Remove directory %s\n", desc);
        }
        else if (strcmp(operation, "RMFILE") == 0)
        {
            fprintf(logging, "Remove file %s\n", desc);
        }
        else if (strcmp(operation, "TOUCH") == 0)
        {
            fprintf(logging, "Created file %s\n", desc);
        }

        fclose(logging);
    }
}


static struct fuse_operations xmp_oper =
{
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .create = xmp_create,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}

```
### Get attribute of file or directory
```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);
    res = lstat(newpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}
```
### Read content of directory
```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    printf("read %s\n", newpath);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(newpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

```
### Read content of a file
```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    printf("read %s\n", newpath);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(newpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}
```
### Restricted
```c
static bool restricted(char *path) {
    bool restricted = false;
    char *copy = strdup(path);
    char *token = strtok(copy, "/");

    while (token != NULL) {
        if (strstr(token, "restricted") != NULL) {
            restricted = true;
        }
        if(strstr(token, "bypass") != NULL){
            restricted = false;
        }
        token = strtok(NULL, "/");
    }

    return restricted;
}

```
### Make new directory
```c
static int xmp_mkdir(const char *path, mode_t mode)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    // Check if the path or any parent directories are restricted
    if (restricted(newpath))
    {
        makeLog("FAILED", "MKDIR", newpath,"");
        return -EACCES; 
    }
    
    int res = mkdir(newpath, mode);

    if (res == -1)
    {
        //means failed
        makeLog("FAILED", "MKDIR", newpath,"");
        return -errno;
    }
    
    //success
    makeLog("SUCCESS", "MKDIR", newpath,"");
    return 0;
}
```
### Remove directory
```c
static int xmp_rmdir(const char *path)
{
    char newpath[1024];
    int res;
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -EACCES;
    }

    res = rmdir(newpath);
    if (res == -1){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -errno;
    }
    //make log success
    makeLog("SUCCESS", "RMDIR", newpath,"");
    return 0;
}
```
### Rename
```c
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char before[1024];
    strcpy(before, src);
    strcat(before, from);

    char new[1024];
    strcpy(new, src);
    strcat(new, to);

    if(restricted(before)){
        if(restricted(new)){
        makeLog("FAILED", "RENAME", before,new);
        return -EACCES;
        }
    }

    res = rename(before, new);

    if (res == -1){
        makeLog("FAILED", "RENAME", before,new);
        return -errno;
    }

    makeLog("SUCCESS", "RENAME", before,new);
    return 0;
}
```
### Remove file specified by path
```c
static int xmp_unlink(const char *path)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -EACCES;
    }

    res = unlink(newpath);

    if (res == -1){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -errno;
    }
    makeLog("SUCCESS", "RMFILE", newpath,"");

    return 0;
}

```
### Create new file by specified path
```c
static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if (restricted(newpath)) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -EACCES;
    }

    int fd = creat(newpath, mode);
    if (fd == -1) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -errno;
    }

    fi->fh = fd;

    makeLog("SUCCESS", "TOUCH", newpath, "");

    return 0;
}
```
### Write log
```c
void makeLog(const char *res, const char *operation, const char *desc, const char *add)
{
    time_t now;
    time(&now);
    struct tm *timestamp = localtime(&now);

    FILE *logging = fopen("/home/fadhlan/target/nanaxgerma/SinSeiFS.log", "a");

    char clock[20];
    strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

    if (logging != NULL)
    {
        fprintf(logging, "%s::%s::%s::%s-", res, clock, operation, desc);

        if (strcmp(operation, "MKDIR") == 0)
        {
            fprintf(logging, "Create directory %s\n", desc);
        }
        else if (strcmp(operation, "RENAME") == 0)
        {
            fprintf(logging, "Rename from %s to %s\n", desc, add);
        }
        else if (strcmp(operation, "RMDIR") == 0)
        {
            fprintf(logging, "Remove directory %s\n", desc);
        }
        else if (strcmp(operation, "RMFILE") == 0)
        {
            fprintf(logging, "Remove file %s\n", desc);
        }
        else if (strcmp(operation, "TOUCH") == 0)
        {
            fprintf(logging, "Created file %s\n", desc);
        }

        fclose(logging);
    }
}
```

# No 5

## Question
> Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.

>- Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.
>- Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.
>- Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.
>> Contoh:
>>- kyunkyun;fbc5ccdf7c34390d07b1f4b74958a9ce
>>- Penjelasan:
>>- kyunkyun adalah username.
>>- fbc5ccdf7c34390d07b1f4b74958a9ce adalah MD5 Hashing dari Subarukun, user kyunkyun menginputkan Subarukun sebagai password. Untuk lebih jelas, kalian bisa membaca dokumentasinya pada https://www.md5hashgenerator.com/. 
>>Catatan: 
>>- Tidak perlu ada password validation untuk mempermudah kalian.		
>- Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.
>> Contoh:
>>- A01_a.pdf
>>- xP4UcxRZE5_A01


>- List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.
>> Contoh: 
>>- result.txt


>>- extension.txt
>>- folder = 12
>>- jpg = 13
>>- png = 2
>>- .
>>- .
>>- .
>>- dan seterusnya

## Source Code

```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <math.h>
#include <openssl/md5.h>


void download_file(const char *url, const char *outputPath){   

    char command[1000];
    sprintf(command, "wget \"%s\" -O \"%s\"", url, outputPath);
    system(command);
}


void unzip_file(const char *outputPath){

    char command[256];
    sprintf(command, "unzip -d . %s && rm rahasia.zip", outputPath);
    system(command);
}

void copyFolder(){
    system("mkdir -p tmp");

    system("cp -R rahasia tmp");
}

static  const  char *dirpath = "/home/fauzan/soal5_4";

int main(int  argc, char *argv[]){

    const char *url = "https://drive.google.com/uc?export=download&id=18YCFdG658SALaboVJUHQIqeamcfNY39a&confirm=t&uuid=7d5965a5-2291-4908-ba42-a15dbef98b89&at=AKKF8vzGZMUfNSeaB1slvn1Hy6ou:1684836666581";
    const char *outputPath = "/home/fauzan/soal5_4/rahasia.zip";

  
    pid_t pid = fork();

    if(pid == 0){ //child process

        download_file(url, outputPath);
    }
    else{

        wait(NULL);

        
        remove("rahasia");

       
        unzip_file(outputPath);

        
        copyFolder();

        }
	
}
```

> Now obviously, this doesn't answer all of the question requirements.

## Explanation

> Downloading the "rahazia.zip" file:
```c 
void download_file(const char *url, const char *outputPath){   

    char command[1000];
    sprintf(command, "wget \"%s\" -O \"%s\"", url, outputPath);
    system(command);
}
```

> Extracting the contents of the zip file and removing the zip file:
```c
void unzip_file(const char *outputPath){

    char command[256];
    sprintf(command, "unzip -d . %s && rm rahasia.zip", outputPath);
    system(command);
}
```

> Copying the file and rename it to temp
```c
void copyFolder(){
    system("mkdir -p tmp");

    system("cp -R rahasia tmp");
} 
```

> Then, in the main function all that we do is calling the function and stated the output path and the download url.
```c
int main(int  argc, char *argv[]){

    const char *url = "https://drive.google.com/uc?export=download&id=18YCFdG658SALaboVJUHQIqeamcfNY39a&confirm=t&uuid=7d5965a5-2291-4908-ba42-a15dbef98b89&at=AKKF8vzGZMUfNSeaB1slvn1Hy6ou:1684836666581";
    const char *outputPath = "/home/fauzan/soal5_4/rahasia.zip";

  
    pid_t pid = fork();

    if(pid == 0){ //child process

        download_file(url, outputPath);
    }
    else{

        wait(NULL);

        
        remove("rahasia");

       
        unzip_file(outputPath);

        
        copyFolder();

        }
	
}
```