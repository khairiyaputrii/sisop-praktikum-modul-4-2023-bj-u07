#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <math.h>
#include <openssl/md5.h>


void download_file(const char *url, const char *outputPath){   

    char command[1000];
    sprintf(command, "wget \"%s\" -O \"%s\"", url, outputPath);
    system(command);
}


void unzip_file(const char *outputPath){

    char command[256];
    sprintf(command, "unzip -d . %s && rm rahasia.zip", outputPath);
    system(command);
}

void copyFolder(){
    system("mkdir -p tmp");

    system("cp -R rahasia tmp");
}

static  const  char *dirpath = "/home/fauzan/soal5_4";

int main(int  argc, char *argv[]){

    const char *url = "https://drive.google.com/uc?export=download&id=18YCFdG658SALaboVJUHQIqeamcfNY39a&confirm=t&uuid=7d5965a5-2291-4908-ba42-a15dbef98b89&at=AKKF8vzGZMUfNSeaB1slvn1Hy6ou:1684836666581";
    const char *outputPath = "/home/fauzan/soal5_4/rahasia.zip";

  
    pid_t pid = fork();

    if(pid == 0){ //child process

        download_file(url, outputPath);
    }
    else{

        wait(NULL);

        
        remove("rahasia");

       
        unzip_file(outputPath);

        
        copyFolder();

        }
	
}

