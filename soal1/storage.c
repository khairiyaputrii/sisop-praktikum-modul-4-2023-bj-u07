#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>


#define MAX_LINE_LENGTH 20000
#define MAX_PLAYERS 20000




typedef struct {
  char name[100];
  int age;
  int potential;
  char club[50];
  char nationality[100];
  char photo[100];
} Player;




int main() {


    // Execute the download command
   if (system("kaggle datasets download -d bryanb/fifa-player-stats-database") != 0) {
       printf("\e[31m\nFailed to download the dataset.\e[0m\n");
       return 1;
   }


   // Execute the extract command
   if (system("unzip fifa-player-stats-database.zip") != 0) {
       printf("\e[31m\nFailed to extract the dataset.\e[0m\n");
       return 1;
   }


   printf("\e[32m\nDataset downloaded and extracted successfully.\e[0m\n");


   FILE *file;
  char line[MAX_LINE_LENGTH];
  Player players[MAX_PLAYERS];
  int numPlayers = 0;


  file = fopen("FIFA23_official_data.csv", "r");
  if (file == NULL) {
     printf("\e[31m\nFailed to open the file.\e[0m\n");
     return 1;
  }


  fgets(line, sizeof(line), file); // Skip the first line (header)


  while (fgets(line, sizeof(line), file)) {
     char *token = strtok(line, ",");
     int fieldCount = 0;
     Player player;


     while (token != NULL && fieldCount <= 8) {
        switch (fieldCount) {
           case 1:
              strncpy(player.name, token, sizeof(player.name));
              break;
           case 2:
              player.age = atoi(token);
              break;
           case 7:
              player.potential = atoi(token);
              break;
           case 8:
              strncpy(player.club, token, sizeof(player.club));
              break;
           case 4:
              strncpy(player.nationality, token, sizeof(player.nationality));
              break;
           case 3:
              strncpy(player.photo, token, sizeof(player.photo));
              break;
           // Add cases for additional fields if needed
        }


        token = strtok(NULL, ",");
        fieldCount++;
     }


     if (fieldCount > 0 && player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
        if (numPlayers >= MAX_PLAYERS) {
           printf("\e[31m\nMaximum number of players reached.\e[0m\n");
           break;
        }
        players[numPlayers++] = player;
     }
  }


  fclose(file);


  for (int i = 0; i < numPlayers; i++) {
   printf("\e[36mName:\e[0m %s\n", players[i].name);
   printf("\e[36mAge:\e[0m %d\n", players[i].age);
   printf("\e[36mPotential:\e[0m %d\n", players[i].potential);
   printf("\e[36mClub:\e[0m %s\n", players[i].club);
   printf("\e[36mNationality:\e[0m %s\n", players[i].nationality);
   printf("\e[36mPhoto:\e[0m %s\n", players[i].photo);
   printf("\e[33m--------------------\e[33m\n");
  }


  return 0;
}
